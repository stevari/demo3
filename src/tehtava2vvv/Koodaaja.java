package tehtava2vvv;

import java.util.ArrayList;

public class Koodaaja {
	private String nimi;
	private int ika;
	private ArrayList<String> ohjelmointikielet;
	
	
	public String getNimi() {
		return nimi;
	}
	public void setNimi(String nimi) {
		this.nimi = nimi;
	}
	public int getIka() {
		return ika;
	}
	public void setIka(int ika) {
		this.ika = ika;
	}
	public ArrayList<String> getOhjelmointikielet() {
		return ohjelmointikielet;
	}

	
	public void lisaaKieli(String kieli) {
		this.ohjelmointikielet.add(kieli);
	}
	
}

