package tehtava2vvv;

import java.util.ArrayList;

public class Ohjelmointitalo {
	private String nimi;
	private int tyontekijaMaara;
	private ArrayList<Koodaaja> tyontekijat;

	public String getNimi() {
		return nimi;
	}

	public void setNimi(String nimi) {
		this.nimi = nimi;
	}

	public boolean lisaaKoodari(Koodaaja koodaaja ) {
		if (!this.tyontekijat.contains(koodaaja)){
			this.tyontekijat.add(koodaaja);
			this.tyontekijaMaara++;
			return true;
		}
		
		return false;
		
	}
}
